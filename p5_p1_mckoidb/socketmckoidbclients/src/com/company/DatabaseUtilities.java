package com.company;

import java.sql.*;

public class DatabaseUtilities {

    private Connection connection = null;

    public boolean ConnectToDatabase() {

        // ******************************************************
        // ****************  Connecting to database  ************
        // ******************************************************

        // Register Mckoi JDBC driver
        try {
            Class.forName("com.mckoi.JDBCDriver"); //.newInstance();
        } catch (Exception e) {
            System.out.println("Cannot register driver: ");
            e.printStackTrace();
            return false;
        }

        // URL for local database with configuration file
        String url = "jdbc:mckoi://localhost:9157/BedrockU.conf?create=false";

        // create root user info
        String admin = "admin";// nota7nLet
        String adminPw = "9999";

        // make a connection to the database
        try {
            connection = DriverManager.getConnection(url, admin, adminPw);
        } catch (SQLException e) {
            System.out.println("cannot connect to database: ");
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public ResultSet getAddresses() {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(
                    "SELECT Student.name, Address.street, Address.city, " +
                            "Address.state, Address.zip " +
                            "FROM Student, Address " +
                            "WHERE Student.id=Address.id"
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getStudents() {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(
                    "SELECT Student.name, Student.id " +
                            "FROM Student"
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getCourses() {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(
                    "SELECT Course.courseId, Course.courseDesc " +
                            "FROM Course"
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getMajors() {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(
                    "SELECT Major.majorId, Major.majorDesc " +
                            "FROM Major"
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getStudentsInMajor(int majorIdIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(String.format(
                    "SELECT Student.name " +
                            "FROM Student " +
                            "WHERE Student.majorId = %d", majorIdIn)
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getStudentsInCourse(int courseIdIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(String.format(
                    "SELECT Student.name " +
                            "FROM Student, Enrolled " +
                            "WHERE Student.id = Enrolled.id AND Enrolled.courseId = %d", courseIdIn)
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getStudentSchedule(int studentIdIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery(String.format(
                    "SELECT courseDesc " +
                            "FROM Enrolled, Course " +
                            "WHERE %d = Enrolled.id AND Course.courseId = Enrolled.courseId", studentIdIn)
            );

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public ResultSet getStudentInfo(int studentIdIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery( String.format(
                    "SELECT Student.name, Address.street, Address.city, " +
                            "Address.state, Address.zip, Major.majorDesc " +
                            "FROM Student, Address, Major " +
                            "WHERE Student.id=Address.id " +
                            "AND %d = Student.id " +
                            "AND Student.majorId = Major.majorId", studentIdIn
            ));

        } catch (SQLException e) {
            System.out.println("Error querying database");
            e.printStackTrace();
            return null;
        }

        return resultset;
    }
    public void addStudent(int studentIdIn, String nameIn, int majorIdIn,
                           String streetIn, String cityIn, String stateIn, String zipIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();
            statement.executeQuery(String.format(
                    "INSERT INTO Student(id, name, majorId) VALUES " +
                            "(%d, '%s', %d ) ", studentIdIn, nameIn, majorIdIn //+
            ));
            System.out.println("Student Added.");
            statement.executeQuery(String.format(
                    "INSERT INTO Address(id, street, city, state, zip) VALUES " +
                    "( %d, '%s', '%s', '%s', '%s')", studentIdIn, streetIn, cityIn, stateIn, zipIn
            ));
            System.out.println("Address Added.");

        } catch (SQLException e) {
            System.out.println("Error adding to Student database");
            e.printStackTrace();
        }

    }
    public void addCourse(int courseIdIn, String courseDescIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        ResultSet resultset = null;
        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            resultset = statement.executeQuery( String.format(
                    "INSERT INTO Course(courseId, courseDesc) VALUES (%d, '%s')", courseIdIn, courseDescIn
            ));
            System.out.println("Course Added.");

        } catch (SQLException e) {
            System.out.println("Error adding to Course database");
            e.printStackTrace();
        }

    }
    public void addMajor(int majorIdIn, String majorDescIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            statement.executeQuery( String.format(
                    "INSERT INTO Major(majorId, majorDesc) VALUES " +
                            "(%d, '%s')", majorIdIn, majorDescIn
            ));
            System.out.println("Major Added.");

        } catch (SQLException e) {
            System.out.println("Error adding to Major database");
            e.printStackTrace();
        }

    }
    public void enrollStudent(int studentIdIn, int courseIdIn) {

        // ******************************************************
        // ****************  Querying database  ****************
        // ******************************************************

        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();

            // query to get addresses of students
            statement.executeQuery( String.format(
                    "INSERT INTO Enrolled(id, courseId) VALUES " +
                            "(%d, '%d')", studentIdIn, courseIdIn
            ));
            System.out.println("Student Enrolled.");

        } catch (SQLException e) {
            System.out.println("Error enrolling Student in Enrolled database");
            e.printStackTrace();
        }

    }
    public void CloseConnectionToDatabase() {

        // close the connection to database
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Could not close connection to database");
            e.printStackTrace();
        }
    }


}
