// André Clay
// C00250294
// CMPS 360
// Programming Project: 5
// Due Date : 2020 04 01
// Program Description:
// Client for BedrockU can list
// student names and id's
// course descriptions and id's
// major descriptions and id's
// students in a given major
// students in a given courseo
// specific student schedule
// specific student personal Info
// add
// new student with address
// new course
// new major
// enroll a student in a course
// Certificate of Authenticity:
// I certify that the code in the given project are entirely my own work.
package com.company;


import java.sql.ResultSet;
import java.util.Scanner;

public class Main {

    public Main() {
    }
    
    public static void main(String[] args) {
        int menuChoice = -1;
        int majorId = -1;
        int courseId = -1;
        int studentId = -1;
        String name = "";
        String city = "";
        String street = "";
        String state = "";
        String zip = "";
        String courseDesc = "";
        String majorDesc = "";
        DatabaseUtilities dbUtils = new DatabaseUtilities();
        Scanner uIn = new Scanner(System.in);
        while (menuChoice == -1) {
            System.out.println("This is the BedrockU client: ");
            System.out.println("Please choose one of the options listed");
            System.out.println("1: list something in the database");
            System.out.println("2: list information on a specific student"); // user enters a specific id
            System.out.println("3: add something");
            System.out.println("4: enroll a student in a course"); // user shown list of course names with ids then can enroll a student by entering course id and student id
            System.out.println("5: quit program");
            System.out.print("Enter chosen option: ");
            menuChoice = uIn.nextInt();
            uIn.nextLine();
            System.out.println();


            switch (menuChoice) {
                case 1:
                    menuChoice = 0;
                    while (menuChoice == 0) {
                        System.out.println("What would you like to list?");
                        System.out.println("1: student names and ids in the database");
                        System.out.println("2: courses in the database with course ids");
                        System.out.println("3: majors in the database with major ids");
                        System.out.println("4: students with a given major"); // user is shown a list of majors then enters major id for list of students to be displayed
                        System.out.println("5: students in a given course"); // user is shown a list of courses then enters course id for list of students to be displayed
                        System.out.print("Enter chosen option: ");
                        menuChoice = uIn.nextInt();
                        uIn.nextLine();
                        System.out.println();

                        switch (menuChoice) {
                            case 1:
                                listStudents(dbUtils);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 2:
                                listCourses(dbUtils);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 3:
                                listMajors(dbUtils);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 4:
                                listMajors(dbUtils);
                                System.out.print("Enter majorID to view students of: ");
                                majorId = uIn.nextInt();
                                uIn.nextLine();
                                System.out.println();
                                listStudentsInMajor(dbUtils, majorId);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 5:
                                listCourses(dbUtils);
                                System.out.print("Enter courseID to view students of: ");
                                courseId = uIn.nextInt();
                                uIn.nextLine();
                                System.out.println();
                                listStudentsInCourse(dbUtils, courseId);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            default:
                                System.out.println("make sure you select a choice listed.");
                                menuChoice = 0;
                        }
                    }
                    break;
                case 2:
                    System.out.print("Enter a StudentId: ");
                    studentId = uIn.nextInt();
                    uIn.nextLine();
                    menuChoice = 0;
                    while (menuChoice == 0) {
                        System.out.println("What about the student would you like to list?");
                        System.out.println("1: student schedule"); // user enters a specific id
                        System.out.println("2: student name, address, and major"); // user enters a specific id
                        System.out.print("Enter chosen option: ");
                        menuChoice = uIn.nextInt();
                        System.out.println();

                        switch (menuChoice) {
                            case 1:
                                listStudentSchedule(dbUtils, studentId);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 2:
                                listStudentInfo(dbUtils, studentId);
                                System.out.println();
                                menuChoice = -1;
                                break;
                            default:
                                System.out.println("make sure you select a choice listed.");
                                menuChoice = 0;
                        }
                    }
                    break;
                case 3:
                    menuChoice = 0;
                    while (menuChoice == 0) {
                        System.out.println("What would you like to add?");
                        System.out.println("1: new student");
                        System.out.println("2: new course");
                        System.out.println("3: new major");
                        System.out.print("Enter chosen option: ");
                        menuChoice = uIn.nextInt();
                        uIn.nextLine();
                        System.out.println();

                        switch (menuChoice) {
                            case 1:
                                System.out.print("Enter new student id: ");
                                studentId = uIn.nextInt();
                                uIn.nextLine();
                                System.out.print("Enter new student name: ");
                                name = uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new student majorId: ");
                                majorId = uIn.nextInt();
                                uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new student Address City: ");
                                city = uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new student Address Street: ");
                                street = uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new student Address State: ");
                                state = uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new student Address Zip: ");
                                zip = uIn.nextLine();
                                if (dbUtils.ConnectToDatabase()) {
                                    dbUtils.addStudent(studentId, name, majorId, street, city, state, zip);
                                    dbUtils.CloseConnectionToDatabase();
                                };
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 2:
                                System.out.print("Enter new course id: ");
                                courseId = uIn.nextInt();
                                uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new course description: ");
                                courseDesc = uIn.nextLine();
                                if (dbUtils.ConnectToDatabase()) {
                                    dbUtils.addCourse(courseId, courseDesc);
                                    dbUtils.CloseConnectionToDatabase();
                                };
                                System.out.println();
                                menuChoice = -1;
                                break;
                            case 3:
                                System.out.print("Enter new major id: ");
                                majorId = uIn.nextInt();
                                uIn.nextLine();
                                System.out.println();
                                System.out.print("Enter new major description: ");
                                majorDesc = uIn.nextLine();
                                if (dbUtils.ConnectToDatabase()) {
                                    dbUtils.addMajor(majorId, majorDesc);
                                    dbUtils.CloseConnectionToDatabase();
                                };
                                System.out.println();
                                menuChoice = -1;
                                break;
                            default:
                                System.out.println("make sure you select a choice listed.");
                                menuChoice = 0;
                        }
                    }
                    break;
                case 4:
                    listCourses(dbUtils);
                    System.out.println();
                    System.out.print("Enter course ID: ");
                    courseId = uIn.nextInt();
                    uIn.nextLine();
                    System.out.print("Enter student ID: ");
                    studentId = uIn.nextInt();
                    uIn.nextLine();
                    System.out.println();
                    if (dbUtils.ConnectToDatabase()) {
                        dbUtils.enrollStudent(studentId, courseId);
                        dbUtils.CloseConnectionToDatabase();
                    };
                    System.out.println();
                    menuChoice = -1;
                    break;
                case 5:
                    System.out.println("Have a nice day.");
                    break;
                default:
                    System.out.println("Please make sure your option is valid.\n");
                    menuChoice = -1;
            }
        }
    }

    public static void listStudents(DatabaseUtilities dbUtils) {
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getStudents();
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("%-20s %-10s\n", "name", "id");
        try {
            String area = "";
            while (results.next()) {
                area += String.format("%-20s %s\n",
                        results.getString(1), results.getString(2));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
    public static void listCourses(DatabaseUtilities dbUtils){
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getCourses();
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("%-20s %-10s\n", "course", "courseID");
        try {
            String area = "";
            while (results.next()) {
                area += String.format("%-20s %4s\n",
                        results.getString(2), results.getString(1));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
    public static void listMajors(DatabaseUtilities dbUtils) {
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getMajors();
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("%-20s %-10s\n", "major", "majorID");
        try {
            String area = "";
            while (results.next()) {
                area += String.format("%-20s %4s\n",
//                                        area += String.format("%-20s \n",
//                                                results.getString(1));
                        results.getString(2), results.getString(1));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
    public static void listStudentsInMajor(DatabaseUtilities dbUtils, int majorId) {
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getStudentsInMajor(majorId);
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("students in majorid %d:\n",majorId );
        try {
            String area = "";
            while (results.next()) {
//                area += String.format("%-20s %4s\n",
//                        results.getString(2), results.getString(1));
                                    area += String.format("%-20s \n",
                                                results.getString(1));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
    public static void listStudentsInCourse(DatabaseUtilities dbUtils, int courseId) {
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getStudentsInCourse(courseId);
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("students in courseId %d:\n",courseId );
        try {
            String area = "";
            while (results.next()) {
//                area += String.format("%-20s %4s\n",
                area += String.format("%-20s \n",
                        results.getString(1));
//                        results.getString(2), results.getString(1));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
    public static void listStudentSchedule(DatabaseUtilities dbUtils, int studentId) {
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getStudentSchedule(studentId);
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("Student %d's Schedule \n", studentId);
        try {
            String area = "";
            while (results.next()) {
                area += String.format("%-20s \n",
                        results.getString(1));
//                area += String.format("%-20s %4s\n",
//                        results.getString(2), results.getString(1));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
    public static void listStudentInfo(DatabaseUtilities dbUtils, int studentId) {
        ResultSet results = null;
        if (dbUtils.ConnectToDatabase()) {
            results = dbUtils.getStudentInfo(studentId);
            dbUtils.CloseConnectionToDatabase();
        };
        System.out.printf("Student %d's Personal Information \n", studentId);
        System.out.printf("%-16s %-10s, %10s, %-5s %-5s %-10s\n", "Name", "Address Street", "City", "State" , "Zip", "Major");
        try {
            String area = "";
            while (results.next()) {
                area += String.format("%-16s %14s, %10s, %-5s %-5s %-10s\n",
                        results.getString(1), results.getString(2), results.getString(3),
                        results.getString(4), results.getString(5), results.getString(6));
            }
            System.out.printf(area);
        } catch (Exception e) {
            System.out.println("bad results");
            e.printStackTrace();
        }
    }
}
