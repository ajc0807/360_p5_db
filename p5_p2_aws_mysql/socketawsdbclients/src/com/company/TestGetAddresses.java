package com.company;

import java.sql.*;

public class TestGetAddresses {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        

        // ******************************************************
        // ****************  Connecting to datatbase  ***********
        // ******************************************************

        // Register MySql JDBC driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); //.newInstance();
        } catch (Exception e) {
            System.out.println("Cannot register driver: " + e);
            return;
        }
        
        // URL for local database with configuration file
//        String url = "jdbc:mysql://---endpoint-url---:---port---/---database---";
        String url = "jdbc:mysql://p5-p2-database-1.cs6xilvns373.us-east-2.rds.amazonaws.com:3306/vectrixrocku";

        // create root user info
//        String admin = "---user-name---";
//        String adminPw = "---user-password---";
        String admin = "rEallycooluserID";
        String adminPw = "Bh7TgDXtxXNG7gQepCzA";


        // make a connection to the database
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, admin, adminPw);
        } catch (SQLException e) {
            System.out.println("cannot connect to database: " + e);
            return;
        }



        // ******************************************************
        // ****************  Querying datatbase  ****************
        // ******************************************************
        
        try {
            // create a Statement object for query execution
            Statement statement = connection.createStatement();
            // create a result object
            ResultSet result;
            
            // query to get addresses of students
            result = statement.executeQuery(
                "SELECT Student.name, Address.street, Address.city, " + 
                        "Address.state, Address.zip " +
                "FROM Student, Address " +
                "WHERE Student.id=Address.id"
            );
            
            // display result of query
            System.out.println("Student Addresses:\n");
            while(result.next()) {
                System.out.println(result.getString(1));
                System.out.println(result.getString(2));
                System.out.println(result.getString(3) + ", " + 
                                   result.getString(4) + "  " +
                                   result.getString(5) + "\n");
            }
            
        } catch (SQLException e) {
            System.out.println("Error querying database: " + e);
            return;
        }
        
    }
    
}
